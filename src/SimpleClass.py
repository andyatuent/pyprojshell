class SimpleClass:
    """Simple place holder class"""

    def __init__(self, name="no name"):
        self.name = name

    def __str__(self):
        return("name[{}]".format(self.name))

if __name__ == '__main__':

    my_car = SimpleClass('Jimmy')
    print(str(my_car))


""" Simple as possible shell main file with command line"""

import click
import logging


logging.basicConfig(level=logging.WARN,format='%(asctime)s %(message)s')
logger=logging.getLogger()


def validate_params(num, def_num, opt_num,verbose, quiet):

    if quiet:
        logger.setLevel(logging.WARNING)
    elif verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logger.info("Parameters: num[{}] def_num[{}] opt_num[{}] verbose[{}] quiet[{}]".format(
        num, def_num, opt_num,verbose, quiet))

    #simple validation rule
    if num >= 0:
        logger.debug("Num is fine - non-negative")
    else:
        msg=f"ERROR: Num param must be non-negetive. Was: {num}"
        logger.error(msg)
        exit()


@click.command()
@click.option('-n', "--num",  required=True, type=int,help="Some numbe mandatory")
@click.option('-d', "--def_num",default="17",show_default=True,help="Some number")
@click.option("-o", "--opt_num",default='',help="(optional) Some number")

#KEEP THESE FOR ALL
@click.option('-v','--verbose',is_flag=True,help='Enables verbose mode')
@click.option('-q','--quiet',is_flag=True,help='Sets log level Enables verbose mode')


def main(num, def_num, opt_num, verbose, quiet):


    validate_params(num, def_num, opt_num, verbose, quiet)
    dbg=12



if __name__ == '__main__':
    main()
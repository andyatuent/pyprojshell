import inspect
import logging

def makeName(self):
    frame= inspect.currentframe()
    return inspect.getframeinfo(frame).function

RULECLASS_POSTFIX = "Rule"


class BaseRule:
    """Simple place holder class"""

    def __init__(self, **kwargs):

        #param defaults - overwrite anything provided by caller
        args = {'confidence':0.5}   #defaults
        args.update(kwargs)

        dbg = 12;
        self.ruleName = args.get('ruleName')
        self.factName = args.get('factName')
        self.confidence = args.get('confidence')

        if not self.ruleName or len(self.ruleName) == 0:
            self.ruleName=self.__class__.__name__

        if not self.factName or len(self.factName) == 0:
            if self.ruleName.endswith(RULECLASS_POSTFIX):
                self.factName= self.ruleName[:-len(RULECLASS_POSTFIX)]
            else:
                msg = f"ERROR: Unable to determine factName. Must either be provided, classname of this class MUST end in \"{RULECLASS_POSTFIX}\""
                raise RuntimeError(msg)
        self.n = makeName(self)
        self.n = self.__class__.__name__

    def toDict(self):
        return{'ruleName':self.ruleName,'factName':self.factName, 'confidence':self.confidence}

    def __str__(self):
        return str(self.toDict())



class IsUUIDRule(BaseRule):
    #pass
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.nx = self.__class__.__name__



if __name__ == '__main__':
    logger = logging.getLogger()



    d = IsUUIDRule(factName='DRule')
    print(str(d))



    d = IsUUIDRule(confidence=0.9)
    print(str(d))
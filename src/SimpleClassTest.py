import inspect

def makeName(self):
    frame= inspect.currentframe()
    return inspect.getframeinfo(frame).function

class BaseRule:
    """Simple place holder class"""

    def __init__(self, ruleName="no name"):
        self.ruleName = ruleName
        self.factName = "factName!"
        self.level = 0

        self.n = makeName(self)
        self.n = self.__class__.__name__

    def toDict(self):
        return{'ruleName':self.ruleName,'factName':self.factName, 'level':self.level}

    def __str__(self):
        return str(self.toDict())



class IsUUIDRule(BaseRule):
    #pass
    def __init__(self, ruleName="no name d"):
        super().__init__(ruleName)

        self.nx = self.__class__.__name__



if __name__ == '__main__':

    rule = BaseRule('Jimmy')
    print(str(rule))


    d = IsUUIDRule('to D')
    print(str(d))



    d = IsUUIDRule()
    print(str(d))
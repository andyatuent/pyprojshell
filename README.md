# pyprojshell

Simple as possible python project shell with
* pipenv
* click command line
* pyunit



# to run
 
 1. pipenv lock
 1. pipenv sync
 1. pipenv shell python ./src/simple_main.py --help

Usage: simple_main.py [OPTIONS]

Options: 

  -n, --num INTEGER   Some numbe mandatory  [required]

  -d, --def_num TEXT  Some number  [default: 17]

  -o, --opt_num TEXT  (optional) Some number

  -v, --verbose       Enables verbose mode

  -q, --quiet         Sets log level Enables verbose mode

  --help              Show this message and exit.
  

4. pipenv shell python ./src/simple_main.py --num=10 